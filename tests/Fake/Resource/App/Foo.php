<?php

namespace BEAR\Middleware\Resource\App;

use BEAR\Resource\ResourceObject;

class Foo extends ResourceObject
{
    public function onGet()
    {
        return 'get';
    }
}
